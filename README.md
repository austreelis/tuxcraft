# Tuxcraft

A simple and accessible modpack for the Louvain-li-Nux minecraft server

**_We have not checked each mod's license yet,_**
**_for now we are just trying to establish a list of mods that works well together (and are stable)._**

**_Using or distributing this modpack may not be allowed, contact me at [dev@austreelis.net](mailto:dev@austreelis.net)) for enquiries._**

I'm aware how sketchy it is to make this repo public with an easy way to package and distribute the modpack.
We consider it okay as long as we don't actually provide a package nor have a public page on modrinth or the likes.

## Support

If you experience a problem or have a request, feel free to [open an issue][tuxcraft-issues].

## Installation

> WIP

## Licensing

This project is licensed under the [Apache 2.0 License][apache20] (see [`LICENSE`](./LICENSE)).)

This project's license doesn't apply on the mods used. We intend to include each mod's license as well as proper credits later.

## Contribution

> Unless you explicitly state otherwise, any contribution intentionally submitted
> for inclusion in the Tuxcraft repo by you, as defined in the Apache-2.0 license, shall be
> licensed as above, without any additional terms or conditions.

[tuxcraft-issues]: http://gitlab.com/austreelis/tuxcraft/issues
[apache20]: http://www.apache.org/licenses/LICENSE-2.0
