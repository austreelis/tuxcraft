{
  inputs = {
    devshell = {
      url = "github:numtide/devshell";
      inputs.nixpkgs.url = "github:nixos/nixpkgs";
    };
    alejandra = {
      url = "github:kamadorueda/alejandra";
      inputs.nixpkgs.follows = "devshell/nixpkgs";
    };
  };

  outputs = {
    alejandra,
    devshell,
    self,
    ...
  } @ inputs: let
    systems = ["x86_64-linux"];

    inherit (devshell.inputs) nixpkgs;

    mkOutputs = system: with nixpkgs.lib; let
      pkgs = import nixpkgs {
        inherit system;
        overlays = [devshell.overlay];
      };

      pkgArgs = inputs // { inherit nixpkgs system; };
    in rec {
      devShells.${system} = rec {
        default = pkgs.devshell.fromTOML "${self}/nix/devshell.toml";
        ci = default;
      };
      packages.${system} = rec {
        tuxcraft-mrpack = pkgs.callPackage "${self}/nix/tuxcraft-mrpack.nix" pkgArgs;
        default = tuxcraft-mrpack;
      };
    };
  in
    with nixpkgs.lib; foldl' recursiveUpdate {} (map mkOutputs systems);
}
