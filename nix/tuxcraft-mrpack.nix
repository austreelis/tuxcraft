{ self
, packwiz
, lib
, stdenv
, drvSrc ? "${self}/modpack"
, packFile ? "pack.toml"
, ...}: with builtins; let
in stdenv.mkDerivation {
  name = "tuxcraft-mrpack";
  version = "2.0.0-alpha2";

  src = drvSrc;

  outputHashMode = "flat";
  outputHashAlgo = "sha256";
  outputHash = lib.fakeSha256;

  buildInputs = [ packwiz ];

  buildPhase = ''
    packwiz mr export \
      --pack-file ${packFile} \
      -o $out
  '';
}
